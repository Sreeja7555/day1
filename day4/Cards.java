package Assessment_day4;

import java.util.Scanner;

abstract class Card {
	protected String holderName;
	protected String cardNumber;
	protected String expiryDate;
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
public Card(){
		
	}
	public Card(String holderName, String cardNumber, String expiryDate) {
		super();
		this.holderName = holderName;
		this.cardNumber = cardNumber;
		this.expiryDate = expiryDate;
	}
	

}
class PaybackCard extends Card{
	private Integer pointsEarned;
	private Double totalAmount;
	public Integer getPointsEarned() {
		return pointsEarned;
	}
	public void setPointsEarned(Integer pointsEarned) {
		this.pointsEarned = pointsEarned;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public PaybackCard(){
		
	}
	public PaybackCard(String holderName, String cardNumber, String expiryDate, Integer pointsEarned,Double totalAmount) {
		super(holderName, cardNumber, expiryDate);
		this.pointsEarned = pointsEarned;
		this.totalAmount = totalAmount;
	}
}
class MembershipCard extends Card{
	private Integer rating;

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public MembershipCard(String holderName, String cardNumber, String expiryDate, Integer rating) {
		super(holderName, cardNumber, expiryDate);
		this.rating = rating;
	}
	public MembershipCard(){
		
	}
}

public class Cards{

	public static void main(String[] args) {
		System.out.println("Select the card\n1.Membership Card\n2.Payback Card");
		Scanner sc=new Scanner(System.in);
		int choice=sc.nextInt();
		if (choice==1){
			sc.nextLine();
			System.out.println("Enter the card details seperated by a |:");
			String info=sc.nextLine();
			System.out.println("Enter the rating");
			Integer rating=Integer.valueOf(sc.nextInt());
			String[] strarr=info.split("\\|");
			MembershipCard mc=new MembershipCard(strarr[0],strarr[1],strarr[2],rating);
			System.out.println(mc.getHolderName()+"'s Membership Card Details");
			System.out.println("Card Number "+mc.getCardNumber());
			System.out.println("Expiry Date "+mc.getExpiryDate());
			System.out.println("Rating "+mc.getRating());
			
		}
		else if(choice==2){
			sc.nextLine();
			System.out.println("Enter the card details seperated by a |:");
			String info=sc.nextLine();
			String[] strarr=info.split("\\|");
			System.out.println("Enter points in card");
			Integer points=Integer.valueOf(sc.nextInt());
			System.out.println("Enter the total amount");
			Double totalAmount=Double.valueOf(sc.nextDouble());
	        PaybackCard pc=new PaybackCard(strarr[0], strarr[1], strarr[2], points, totalAmount);
	        System.out.println(pc.getHolderName()+"'s Payback Card Details:\n"+"Card Number "+pc.getCardNumber());
	        System.out.println("Expiry Date "+pc.getExpiryDate());
	        System.out.println("Points Earned "+pc.getPointsEarned());
	        System.out.println("Total Amount "+pc.getTotalAmount());
		}
		sc.close();
	}

	}


