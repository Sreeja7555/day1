package Assessment_day4;
class ABC
{
int a = 100;
public void display() {
System.out.printf("a in A = %d\n", a);
}
}
class B extends ABC{
int b=200;
public void display(){
System.out.printf("b in B = %d\n",b);
}
}
class C extends B{
int c=300;
public void display(){
System.out.printf("c in C =%d\n",c);
}
}
public class main_6 {
   static int a=555;
	public static void main(String[] args) {
		ABC objA = new ABC(); 
		B objB1 = new B(); 
		ABC objB2 = new B();
		C objC1 = new C(); 
		B objC2 = new C(); 
		ABC objC3 = new C(); 
		objA.display(); 
		objB1.display();
		objB2.display(); 
		objC1.display(); 
		objC2.display(); 
		objC3.display();
	}
}
