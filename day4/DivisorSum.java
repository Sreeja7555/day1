package Assessment_day4;

import java.util.Scanner;

interface AdvancedArithmetic {
	int divisor_sum(int n);

}

class MyCalculator implements AdvancedArithmetic{
	public int divisor_sum(int x) {
		int sum=0;
		for(int i=1;i<=x;i++) {
			if(x%i==0) {
				sum=sum+i;
			}
		}
		return sum;
	}
}
public class DivisorSum {

	public static void main(String[] args) {
		MyCalculator c=new MyCalculator();
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the number");
		int n=sc.nextInt();
		System.out.println(c.divisor_sum(n));
		sc.close();

		
	}

}
