package Assessment_day4;

import java.util.Scanner;

abstract class Shape{
	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
public Shape(String name){
	this.name=name;
}
abstract Float calculateArea();
}
class Square extends Shape{
	private Integer side;

	public Integer getSide() {
		return side;
	}

	public void setSide(Integer side) {
		this.side = side;
	}
	public Float calculateArea(){
		Float area=Float.valueOf((float)side*side);
		return area;
		
	}
	public Square(String name,Integer side){
		super(name);
		this.side=side;
	}
	}
	class Circle extends Shape {
		private Integer radius;
		
		public Integer getRadius() {
			return radius;
		}
		public void setRadius(Integer radius) {
			this.radius = radius;
		}
		public Float calculateArea(){
			Float area=Float.valueOf(radius*radius*3.14f);
			return area;
		}
		public Circle(String name,Integer radius){
			super(name);
			this.radius=radius;
		}
		}
		class Rectangle extends Shape{
			private Integer length;
			private Integer width;
			public Integer getLength() {
				return length;
			}
			public void setLength(Integer length) {
				this.length = length;
			}
			public Integer getWidth() {
				return width;
			}
			public void setWidth(Integer width) {
				this.width = width;
			}
			public Float calculateArea(){
				Float area=Float.valueOf((float)length*width);
				return area;
			}
			public Rectangle(String name,Integer length,Integer width){
				super(name);
				this.length=length;
				this.width=width;
			}
			
		}
public class main_1 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the shape name");
		System.out.println("Circle\nRectangle\nSquare");
		String choice=sc.next();
		if (choice.equals("Circle")){
			System.out.println("Enter the radius");
			int raidus=sc.nextInt();
			Circle cir1=new Circle("Cir1",raidus);
			System.out.printf("%.2f",cir1.calculateArea());
		}
		else if (choice.equals("Rectangle")){
			System.out.println("Enter the length");
			int length=sc.nextInt();
			System.out.println("Enter the width");
			int width=sc.nextInt();
			Rectangle rect1=new Rectangle("Rect1",length,width);
			System.out.printf("%.2f",rect1.calculateArea());
		}
		else if (choice.equals("Square")){
			System.out.println("Enter the side");
			int side=sc.nextInt();
			Square sq1=new Square("Sq1",side);
			System.out.printf("%.2f",sq1.calculateArea());
		}
		sc.close();



	}

}

	


