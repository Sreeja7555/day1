package assessment_day2;

import java.util.Scanner;

public class MiddleCharacter {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        System.out.print("Input a string:\n");
        String str = in.nextLine();
        System.out.print("The middle character in the string: " + middlecharacter(str)+"\n");
    }
 public static String middlecharacter(String str)
    {
        int pos;
        int len;
        if (str.length() % 2 == 0)
        {
            pos = str.length() / 2 - 1;
            len= 2;
        }
        else
        {
            pos = str.length() / 2;
            len= 1;
        }
        return str.substring(pos, pos + len);
	}

}

