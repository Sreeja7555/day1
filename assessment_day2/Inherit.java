package assessment_day2;
class a
{
	public void show1()
	{
		System.out.println("Inheritance");
	}
}
class b extends a
{
	 public void show() {
		System.out.println("Polymorphism");
	}
}
public class Inherit {

	public static void main(String[] args) {
		a m=new a();
		b n=new b();
		m.show1();
		n.show();
	}

}
