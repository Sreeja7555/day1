package assessment_day5;

import java.util.Scanner;
 public class InputMismatchException {

	public static void main(String[] args) throws ArithmeticException {
 				try {
				Scanner sc=new Scanner(System.in);
				System.out.println("Enter the first input");
				int numerator=sc.nextInt();
				System.out.println("Enter the second input");
				int denominator=sc.nextInt();
				int result=numerator/denominator;
				System.out.println("The result is "+result);
				sc.close();
				}
				catch (Exception ime) {
					System.out.println(ime);
				}
			}

	}


