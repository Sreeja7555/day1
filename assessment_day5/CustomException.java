package assessment_day5;

import java.util.Scanner;

class CustomExceptionDemo extends Exception
{
	CustomExceptionDemo(String str)
{
super(str);
}
}

public class CustomException {

	public static void main(String[] args) throws CustomExceptionDemo {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Player name:");
		String name=sc.nextLine();
		System.out.println("Enter the Player age :");
		int age=sc.nextInt();
		if(age<19)
		{
		throw new CustomExceptionDemo("InvalidAgeRangeException");
		}
		else
		{
			System.out.println("Player name :"+name);
			System.out.println("Player age :"+age);
		}

	}

}
