package assessment_day5;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.InputMismatchException;
 public class RunRate {

	public static void main(String[] args) throws IOException {
		 
 
 				BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
				DecimalFormat dc=new DecimalFormat("##.##");
				try 
				{
				System.out.println("Enter the total runs scored");
				int runs=Integer.parseInt(br.readLine());
				System.out.println("Enter the total overs faced");
				int overs=Integer.parseInt(br.readLine());
				double runRate=(double)runs/(double)overs;
				System.out.println("Current Run Rate : "+ dc.format(runRate));
				}
				catch(ArithmeticException ae) {
					System.out.println(ae);
				}
				catch(InputMismatchException ime) {
					System.out.println(ime);
				}
				catch(NumberFormatException e) {
					System.out.println(e);
				}
			}

	}


