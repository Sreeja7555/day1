package assessment_day1;

import java.util.Scanner;

public class Average {

	public static void main(String[] args) {
		 Scanner sc = new Scanner(System.in);
	        System.out.print("Enter the first number: ");
	        double a = sc.nextDouble();
	        System.out.print("Enter the second number: ");
	        double b = sc.nextDouble();
	        System.out.print("Enter the third number: ");
	        double c = sc.nextDouble();
	        double avg=(a+b+c)/3;
	        System.out.print("The average of the numbers is : " + avg);

	}

}
