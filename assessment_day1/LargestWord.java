package assessment_day1;

import java.util.Scanner;

class UserMain{
	public static String getLongestWord(String x){
		String[] strarr=x.split(" ");
		String maxstr=strarr[0];
		for (int i=0;i<strarr.length;i++)
		{
			if (strarr[i].length()>maxstr.length()){
				maxstr=strarr[i];
			}
		}
		return maxstr;
	}
}

public class LargestWord {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		System.out.println(UserMain.getLongestWord(str));
		sc.close();

	}

}