package assessment_day1;

import java.util.Scanner;

public class Arithmetic {

	public static void main(String[] args) {
		int a,b,add,sub,mul;
		double div,rem;
		 Scanner sc = new Scanner(System.in);
	
		 System.out.print("Enter two numbers : ");
		 a = sc.nextInt();
		    b = sc.nextInt();
		    add=a+b;
		    sub=a-b;
		    mul=a*b;
		    div=(double)a/b;
		    rem=(double)a%b;

 		    System.out.println("Sum of two numbers= " + add);
		    System.out.println("Difference between two numbers= " + sub);
		    System.out.println("Multiplication of two numbers= " + mul);
		    System.out.println("Division of two numbers= " + div);
		    System.out.println("remainder of two numbers ="+rem);

	}

}
