package assessment_day1;

import java.util.Scanner;

class CheckingTheSum
{
	
	public static int checkSum(int num)
	{
		int rem;
		int sum=0;
		if (num>0)
		{
		   while (num>0)
		   {
			   rem=num%10;
			   num=num/10;
			   sum=sum+rem;
		   }
		if (sum%2==0){
			return -1;
		}
		else{
			return 1;
		}
		
		}
		else
		{
			return 0;
		}
    }


}
public class CheckSum {

	public static void main(String[] args) 
	{
 			 
           Scanner sc=new Scanner(System.in);
			int a=sc.nextInt();
			int temp=CheckingTheSum.checkSum(a);
			if (temp==-1){
			System.out.println("Sum of the digits is even");
			}
			else if(temp==1){
				System.out.println("Sum of the digits is odd");
			}
			else if (temp==0){
				System.out.println("Please enter a positive number");
			}
			sc.close();

	}

}
