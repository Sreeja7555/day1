package assessment_day1;

import java.util.Scanner;

class SumOfSquares{
	public static int sumOfSquaresOfEvenDigits(int num){
		int rem;
		int sumofsq=0;
		if (num>0){
			while (num>0)
			{
				rem=num%10;
				num=num/10;
				if (rem%2==0){
					sumofsq=sumofsq+(rem*rem);
				}
			}
			if(sumofsq==0){
				return 1;
			}
			else{
				return sumofsq;
			}
		}
		else {
			return 0;
		}
	}

}
public class UserMainCode
{

	public static void main(String[] args) 
	{
        Scanner sc=new Scanner(System.in);
		
		int n=sc.nextInt();
		int x=SumOfSquares.sumOfSquaresOfEvenDigits(n);
		if (x==0)
		{
			System.out.println("Enter a positive number");
		}
		else if(x==1)
		{
			System.out.println("No even digits..0");
		}
		else{
			System.out.println(x);
			sc.close();
		}
	}

	 

 	}




	






