1.Write a query in SQL to display the first name, last name, department number, and department name for each employee
query:
mysql> SELECT E.first_name,E.last_name,D.department_id, D.department_name FROM employees E JOIN departments D  ON E.department_id = D.department_id;
+------------+------------+---------------+-----------------+
| first_name | last_name  | department_id | department_name |
+------------+------------+---------------+-----------------+
| Valli      | Vpatabal   |            60 | IT              |
| David      | Daustin    |            60 | IT              |
| Bruce      | Bernst     |            60 | IT              |
| Alexander  | Ahunold    |            60 | IT              |
| Lex        | Ldehaan    |            90 | Executive       |
| Neena      | Nkochaaar  |            90 | Executive       |
| Steven     | sking      |            90 | Executive       |
| William    | Wgietz     |           110 | Accounting      |
| Shelley    | Shiggins   |           110 | Accounting      |
| Diana      | Dlorentz   |           110 | Accounting      |
+------------+------------+---------------+-----------------+
10 rows in set (0.08 sec)

2.Write a query in SQL to display the first name, last name, department number and department name, for all employees for departments 80 or 40
Query:
mysql> SELECT E.first_name,E.last_name,D.department_id, D.department_name FROM employees E JOIN departments D  ON E.department_id = D.department_id and e.department_id in(80,40);
Empty set (0.00 sec)

3.Write a query in SQL to display the first name of all employees including the first name of their manager
Query:
mysql> select e.first_name as "Employee_name",m.first_name as "Manager_name" from employees e join departments d on (e.department_id=d.department_id) join employees m on(m.employee_id=d.manager_id);
+---------------+--------------+
| Employee_name | Manager_name |
+---------------+--------------+
| Steven        | Steven       |
| Neena         | Steven       |
| Lex           | Steven       |
| Alexander     | Alexander    |
| Bruce         | Alexander    |
| David         | Alexander    |
| Valli         | Alexander    |
| Diana         | Shelley      |
| Shelley       | Shelley      |
| William       | Shelley      |
+---------------+--------------+
10 rows in set (0.04 sec)

4.Write a query in SQL to display all departments including those where does not have any employee
Query:
mysql> select d.department_name FROM employees e RIGHT OUTER JOIN departments d ON e.department_id = d.department_id;
+------------------+
| department_name  |
+------------------+
| Administration   |
| Marketing        |
| Purchasing       |
| Human Resources  |
| Shipping         |
| IT               |
| IT               |
| IT               |
| IT               |
| Public Relations |
| Sales            |
| Executive        |
| Executive        |
| Executive        |
| Finance          |
| Accounting       |
| Accounting       |
| Accounting       |
+------------------+
18 rows in set (0.00 sec)

5.Write a query in SQL to display the first name, last name, department number and name, for all employees who have or have not any department
Query:
mysql> SELECT E.first_name, E.last_name, E.department_id, D.department_name
    ->   FROM employees E
    ->    LEFT OUTER JOIN departments D
    ->      ON(E.department_id = D.department_id);
+------------+-----------+---------------+-----------------+
| first_name | last_name | department_id | department_name |
+------------+-----------+---------------+-----------------+
| Steven     | sking     |            90 | Executive       |
| Neena      | Nkochhar  |            90 | Executive       |
| Lex        | Ldehaan   |            90 | Executive       |
| Alexander  | Ahunold   |            60 | IT              |
| Bruce      | Bernst    |            60 | IT              |
| David      | Daustin   |            60 | IT              |
| Valli      | Vpatabal  |            60 | IT              |
| Diana      | Dlorentz  |           110 | Accounting      |
| Shelley    | Shiggins  |           110 | Accounting      |
| William    | Wgietz    |           110 | Accounting      |
+------------+-----------+---------------+-----------------+
10 rows in set (0.00 sec)





		
		
			      
		
